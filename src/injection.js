import { StairwayLayer } from './StairwayLayer.js'
import { StairwayDocument } from './StairwayDocument.js'
import { Stairway } from './Stairway.js'
import { StairwayControl } from './StairwayControl.js'
import { StairwayConfig } from './StairwayConfig.js'
import { BaseStairway } from './BaseStairway.js'

const fields = foundry.data.fields

export const injectStairways = () => {
  // register stairway classes
  CONFIG.Stairway = {
    documentClass: StairwayDocument,
    objectClass: Stairway,
    layerClass: { group: 'interface', layerClass: StairwayLayer },
    sheetClasses: { base: StairwayConfig }
  }

  DocumentSheetConfig.registerSheet(StairwayDocument, 'stairway', StairwayConfig, { makeDefault: true })

  hookCanvas()
  hookBaseScene()
  hookSceneData()
  hookControlsLayer()
  hookTokenLayer()

  // add stairways as embedded document for existing scenes
  for (const scene of game.data.scenes) {
    scene.stairways = foundry.utils.duplicate(scene.flags.stairways || [])
  }

  // Hook createScene and add stairways as embedded document
  Hooks.on('createScene', (scene, options, userId) => {
    scene.data.stairways = foundry.utils.duplicate(scene.data.flags.stairways || [])
  })
}

const hookCanvas = () => {
  // inject StairwayLayer into the canvas layers list
  const origLayers = CONFIG.Canvas.layers
  CONFIG.Canvas.layers = Object.keys(origLayers).reduce((layers, key, i) => {
    layers[key] = origLayers[key]

    // inject stairways layer after walls
    if (key === 'walls') layers.stairways = CONFIG.Stairway.layerClass

    return layers
  }, {})

  // FIXME: workaround for #23
  if (!Object.is(Canvas.layers, CONFIG.Canvas.layers)) {
    console.error('Possible incomplete layer injection by other module detected! Trying workaround...')

    const layers = Canvas.layers
    Object.defineProperty(Canvas, 'layers', {
      get: function () {
        return foundry.utils.mergeObject(CONFIG.Canvas.layers, layers)
      }
    })
  }

  // Hook the Canvas.getLayerByEmbeddedName
  const origGetLayerByEmbeddedName = Canvas.prototype.getLayerByEmbeddedName
  Canvas.prototype.getLayerByEmbeddedName = function (embeddedName) {
    if (embeddedName === 'Stairway') {
      return this.stairways
    } else {
      return origGetLayerByEmbeddedName.call(this, embeddedName)
    }
  }
}

const hookBaseScene = () => {
  // inject Stairway into scene metadata
  const BaseScene = foundry.documents.BaseScene
  const sceneMetadata = Object.getOwnPropertyDescriptor(BaseScene.prototype.constructor, 'metadata')
  // Hook the BaseScene#metadata getter
  Object.defineProperty(BaseScene.prototype.constructor, 'metadata', {
    get: function () {
      const metadata = sceneMetadata.get.call(this)
      metadata.embedded.Stairway = BaseStairway

      return metadata
    }
  })

  // add stairways getter
  Object.defineProperty(BaseScene.prototype, 'stairways', {
    get: function () {
      return this.data.stairways
    }
  })
}

const hookSceneData = () => {
  // inject BaseStairway into SceneData schema
  const SceneData = foundry.data.SceneData
  const sceneSchema = SceneData.prototype.constructor.defineSchema
  // Hook the SceneData#defineSchema getter
  SceneData.prototype.constructor.defineSchema = function () {
    const schema = sceneSchema.call(this)
    schema.stairways = fields.embeddedCollectionField(BaseStairway)

    return schema
  }
}

const hookControlsLayer = () => {
  // Hook ControlsLayer.draw
  const origDraw = ControlsLayer.prototype.draw
  ControlsLayer.prototype.draw = function () {
    this.drawStairways()
    origDraw.call(this)
  }
  ControlsLayer.prototype.drawStairways = function () {
    // Create the container
    if (this.stairways) this.stairways.destroy({ children: true })
    this.stairways = this.addChild(new PIXI.Container())

    // Iterate over all stairways
    for (const stairway of canvas.stairways.placeables) {
      this.createStairwayControl(stairway)
    }

    this.stairways.visible = !canvas.stairways._active
  }
  ControlsLayer.prototype.createStairwayControl = function (stairway) {
    const sw = this.stairways.addChild(new StairwayControl(stairway))
    sw.visible = false
    sw.draw()
  }
}

const hookTokenLayer = () => {
  // Hook TokenLayer.activate / deactivate
  const origActivate = TokenLayer.prototype.activate
  TokenLayer.prototype.activate = function () {
    origActivate.call(this)
    if (canvas.controls) canvas.controls.stairways.visible = true
  }

  const origDeactivate = TokenLayer.prototype.deactivate
  TokenLayer.prototype.deactivate = function () {
    origDeactivate.call(this)
    if (canvas.controls) canvas.controls.stairways.visible = false
  }
}
